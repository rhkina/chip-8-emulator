## Chip-8 Emulator
Based on [Laurence Muller tutorial](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/) and [moises' blog article](http://omokute.blogspot.com/2012/06/emulation-basics-write-your-own-chip-8.html).

This guide is intended to give myself an introduction to the world of emulation and I will try to write one myself from scratch.

### Emulator
It’s important to first understand what an emulator is and isn’t.
An emulator is a computer program that mimics the internal design and functionality of a computer system (System A). It allows users to run software designed for this specific system (Sytem A) on a totally different computer system or architecture (System B).
Often people confuse a simulator with an emulator and vice versa. Just remember that these words aren’t synonyms.
A simulator is just a clone of an application and does not allow the computer run other applications made for the original computer.
By the other hand, an emulator is a program that mimics the hardware of original computer and therefore allow us to run all applications made for the original computer in the emulator computer.

### Chip-8
Writing a Chip 8 emulator is probably the easiest emulation project you can undertake. Due to small number of opcodes (35 in total for Chip 8 ) and the fact that a lot of instructions are used in more advanced CPUs, a project like this is educational (get a better understanding of how the CPU works and how machine code is executed), manageable (small number of opcodes to implement) and not too time consuming (project can be finished in a few days).

### CPU specifications
When you start writing an emulator, it is important to find as much information as possible about the system we want to emulate. 
Try to find out how much memory and registers are used in the system, what architecture it is using and see if you can get hold of technical documents that describe the instruction set.
I have found a very good technical explanation at [Cogwood's reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM).
Basically, all machines are made up of 4 parts:
1. Input
2. Output
3. CPU
4. Memory

#### Input
For the CHIP8 virtual machine, the input comes from a 16-button keyboard (pretty convenient that the number of keys falls within a nibble). The machine is also fed with the programs it is supposed to run.
In python, we just need to store key input states and check these per cycle:
`self.key_inputs = [0]*16`

The computers which originally used the Chip-8 Language had a 16-key hexadecimal keypad with the following layout:
```
+-------+-------+-------+-------+
|   1   |   2   |   3   |   C   |
+-------+-------+-------+-------+
|   4   |   5   |   6   |   D   |
+-------+-------+-------+-------+
|   7   |   8   |   9   |   E   |
+-------+-------+-------+-------+
|   A   |   0   |   B   |   F   |
+-------+-------+-------+-------+
```

This emulator will use the following mapping:
```
Emulator
+-------+-------+-------+-------+
|   1   |   2   |   3   |   4   |
+-------+-------+-------+-------+
|   Q   |   W   |   E   |   R   |
+-------+-------+-------+-------+
|   A   |   S   |   D   |   F   |
+-------+-------+-------+-------+
|   Z   |   X   |   C   |   V   |
+-------+-------+-------+-------+

                |
                V
Chip-8 System
+-------+-------+-------+-------+
|   1   |   2   |   3   |   C   |
+-------+-------+-------+-------+
|   4   |   5   |   6   |   D   |
+-------+-------+-------+-------+
|   7   |   8   |   9   |   E   |
+-------+-------+-------+-------+
|   A   |   0   |   B   |   F   |
+-------+-------+-------+-------+

```
In addition to this, you can also speed up and slow down emulation using the + and - keys.

#### Output
For output, the machine uses a 64x32 display, and a simple sound buzzer. The display is basically just an array of pixels that are either in the on or off state:
`self.display_buffer = [0]*32*64 # 64*32`

The original implementation of the Chip-8 language used a 64x32-pixel monochrome display with this format:
```
+-------------+-------------+
|    (0,0)    |   (63,0)    |
+-------------+-------------+
|    (0,31)   |   (63,31)   |
+-------------+-------------+
```

Chip-8 draws graphics on screen through the use of sprites. A sprite is a group of bytes which are a binary representation of the desired picture. Chip-8 sprites may be up to 15 bytes, for a possible sprite size of 8x15.


#### CPU and Memory
Now for the "hard" part. The Chip 8 CPU has several parts of note, and all are important to understand how it works. I'll run through them here, but for more info see the [Cogwood's reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM).

**Memory**

CHIP8 has memory that can hold up to 4KB (4096 bytes) or RAM, from location 0x000 (0) to 0xFFF (4095). This includes the interpreter itself, the fonts (more on this later), and where it loads the program it is supposed to run (from input). In python, this will be:
`self.memory = [0]*4096 # max 4096`

Memory Map:
```
+---------------+= 0xFFF (4095) End of Chip-8 RAM
|               |
|               |
|               |
|               |
|               |
| 0x200 to 0xFFF|
|     Chip-8    |
| Program / Data|
|     Space     |
|               |
|               |
|               |
+- - - - - - - -+= 0x600 (1536) Start of ETI 660 Chip-8 programs
|               |
|               |
|               |
+---------------+= 0x200 (512) Start of most Chip-8 programs
| 0x000 to 0x1FF|
| Reserved for  |
|  interpreter  |
+---------------+= 0x000 (0) Start of Chip-8 RAM
```

**Registers**

The CHIP8 has 16 8-bit registers (usually referred to as Vx where x is the register number in [Cogwood's reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM). These are generally used to store values for operations. The last register, Vf, is mostly used for flags and should be avoided for use in programs. In python, we will be storing our register values as:
`self.gpio = [0]*16 # 16 zeroes`

It also has 2 timer registers, that basically just cause delays by decrementing to 0 for each cycle, wasting an operation in the process. Again, in python, this will be as simple as defining two variables that we'll decrement per cycle.

```
self.sound_timer = 0
self.delay_timer = 0
```

It has an index register which is 16-bit.
`self.index = 0`

The program counter is also 16-bit.
`self.pc = 0`

There is also a stack pointer, which includes the address of the topmost stack element. The stack has at most 16 elements in it at any given time. Since we're doing this in python, which has a list that we can just pop/append to, we can ignore this. In python, just make a list:
`self.stack = []`

### The code
For our input/output, we are going to use pyglet, a module for python that wraps around OpenGL boilerplate so we can easily output graphics. It can also handle sound output. And oh, it does keyboard handling too! Now all that's left is the CPU.
